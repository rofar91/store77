# -*- coding: utf-8 -*-
import scrapy
import logging


class StoreSpider(scrapy.Spider):
    name = 'store'
    allowed_domains = ['store77.net']
    start_urls = ['https://store77.net/telefony_apple/']

    logging.basicConfig(filename='loger.log', level=logging.DEBUG)
    logging.debug('This message should go to the log file')
    logging.info('So should this')
    logging.warning('And this, too')

    def parse(self, response):
        for items in response.xpath('//div[@class="blocks_product"]/div[@class="blocks_product_fix_w"]/a/@href'):
            yield response.follow(items, self.parse_card)

        #   next page
        # for arrow in response.xpath('//div[@class="wrap_pagination"]/ul[@class="pagination"]//li[@class="pagin_arrow"]/a/@href'):
        #     print(arrow)
        #     yield response.follow(arrow, self.parse)

    def parse_card(self, response):
        params = {
            'Ссылка': response.url,
            'Артикул': response.xpath('//a[@class="link_but link_title_cart card-product-special-buy-button"]//@data-id').get(),
            # 'Название': response.xpath('//div[@class="block_title_card_product"]/h1[@class="title_card_product"]/text()').get(),
            # 'Модель': response.xpath('').get(),
            # 'Цена': response.xpath('//div[@class="block_title_card_product"]/p[@class="price_title_product"]/text()').get(),
            # 'Описание': response.xpath('//div[@class="description_b"]').get(),
            # 'Характеристики': response.xpath('//div[@class="item"]').get(),
            # 'Размер (объем)': response.xpath('').get(),
            'Цвет (html code)  (фильтр)': response.xpath('//div[@class="color_radio"]//span[@class="no_bor-r active"]/@style').get(),
            'Цвета (перелинковка)': response.xpath('//div[@class="color_radio"]//span[@class="no_bor-r "]/@style').getall(),
            'Цена': response.xpath('//div[@class="block_title_card_product"]/p[@class="price_title_product"]/text()').get(),
            # 'Диагональ  (фильтр)': response.xpath('').get(),
            # 'Размеры (перелинковка)': response.xpath('').get(),
            # 'Товары к размерам (артикулы)': response.xpath('').get(),
            # 'Товары к цветам (артикулы)': response.xpath('').get(),
            # 'Категория1': response.xpath('').get(),
            # 'Категория2': response.xpath('').get(),
            # 'Категория3': response.xpath('').get(),
            # 'Категория4': response.xpath('').get(),
            # 'Категория5': response.xpath('').get(),
            # 'Главное фото': response.xpath('').get(),
            # 'Дополнительные фото': response.xpath('').get(),
        }

        yield params

